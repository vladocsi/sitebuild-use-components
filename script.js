Prism.plugins.NormalizeWhitespace.setDefaults({
    'remove-trailing': true,
    'remove-indent': true,
    'left-trim': true,
    'right-trim': true,
});

var pre = document.getElementsByTagName('pre');
for (var i = 0; i < pre.length; i++) {
    var isLanguage = pre[i].children[0].className.indexOf('language-');
    if ( isLanguage === 0 ) {
        var button = document.createElement('button');
        button.className = 'btn-clipboard';
        button.textContent = 'Copy';

        pre[i].appendChild(button);
    }
}

var copyCode = new Clipboard('.btn-clipboard', {
    target: function(trigger) {
        return trigger.previousElementSibling;
    }
});

copyCode.on('success', function(event) {
    event.clearSelection();
    event.trigger.textContent = 'Copied';
    window.setTimeout(function() {
        event.trigger.textContent = 'Copy';
    }, 2000);
});

copyCode.on('error', function(event) {
    event.trigger.textContent = 'Press "Ctrl + C" to copy';
    window.setTimeout(function() {
        event.trigger.textContent = 'Copy';
    }, 2000);
});



$(function () {

    let $button = $('#menu-button');
    let $menu = $('#context-nav');
    let $container = $('#container');
    let $contextnavitems =  $('#context-nav-items');

    $('[data-toggle="popover-user"]').popover({
        html: true,
        placement: "bottom",
        content: function () {
            return $('#popover-content-user').html();
        }
    });

    $button.on('click touch', function () {
        if (!$menu.hasClass('opened-context-nav')) {
            $menu.removeClass('closed-context-nav');
            $menu.removeClass('closed-context-nav-xs');
            $container.removeClass('ml-80');
            $menu.addClass('opened-context-nav');
            $menu.addClass('opened-context-nav-xs');
            $container.addClass('ml-345');
            $contextnavitems.removeClass('d-none');
            $contextnavitems.addClass('d-block');


        } else {
            $menu.removeClass('opened-context-nav');
            $menu.removeClass('opened-context-nav-xs');
            $container.removeClass('ml-345');
            $menu.addClass('closed-context-nav');
            $menu.addClass('closed-context-nav-xs');
            $container.addClass('ml-80');
            $contextnavitems.removeClass('d-block');
            $contextnavitems.addClass('d-none');
        }

    });

});

$(function(){
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});

$('[data-toggle="popover-info"]').popover({
    html: true,
    placement: "left",
    content: function () {
        return $('#popover-content-info').html();
    }
});
